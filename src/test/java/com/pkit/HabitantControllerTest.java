package com.pkit;

import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.pkit.model.*;
import com.pkit.service.*;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = Application.class)
public class HabitantControllerTest {

    private final ObjectMapper om = new ObjectMapper()
            .registerModule(new ParameterNamesModule())
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .setDateFormat(new StdDateFormat());

    protected MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    HabitantService habitantService;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetList() throws Exception {
        log.debug("testGetList: ");

        MvcResult mvcResult = this.mockMvc.perform(get("/habitant/"))
                .andExpect(status().isOk()).andReturn();

        List<Habitant> habitantList = om.readValue(mvcResult.getResponse().getContentAsByteArray(), new TypeReference<List<Habitant>>() {});

        log.debug("habitantList: ");
        for (Habitant c : habitantList)
            log.debug(c.toString());
        Assert.assertEquals(7L, habitantList.size());
    }

    @Test
    public void testAdd() throws Exception {

        Habitant habitant = newHabitant();
        log.debug("testAdd: {}", habitant);

        String requestBody = om.writeValueAsString(habitant);
        MvcResult mr = this.mockMvc.perform(post("/habitant/").content(requestBody).contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        habitant = om.readValue(mr.getResponse().getContentAsString(), Habitant.class);
        log.debug("habitant: {}", habitant);

        Assert.assertNotNull(habitant.getHabitantId());
        habitantService.delete(habitant.getHabitantId());
    }

    @Test
    public void testUpdate() throws Exception {
        log.debug("testUpdate: ");

        Habitant habitant = newHabitant();

        log.debug("habitant: {}", habitant);

        String newDocNumber = "87654321";
        habitant = habitantService.add(habitant);
        habitant.setDocNumber(newDocNumber);
        log.debug("habitant: {}", habitant);

        String requestBody = om.writeValueAsString(habitant);
        MvcResult mr = this.mockMvc
                .perform(put("/habitant/{habitantId}", habitant.getHabitantId())
                        .content(requestBody)
                        .contentType("application/json"))
                .andExpect(status().isOk()).andReturn();
        Habitant newHabitant = om.readValue(mr.getResponse().getContentAsString(), Habitant.class);

        log.debug("newHabitant: {}", newHabitant);

        Assert.assertEquals(newDocNumber, newHabitant.getDocNumber());

        habitantService.delete(newHabitant.getHabitantId());
    }

    @Test
    public void testDelete() throws Exception {
        log.debug("testDelete: ");

        Habitant habitant = newHabitant();

        habitant = habitantService.add(habitant);

        log.debug("habitant: {}", habitant);

        MvcResult mr = this.mockMvc.perform(delete("/habitant/{habitantId}", habitant.getHabitantId()))
                .andExpect(status().isOk()).andReturn();

        Assert.assertFalse(habitantService.exists(habitant.getHabitantId()));
    }

    @Test
    public void testValidation() throws Exception {
        log.debug("testDocNumberValidation: ");

        Habitant habitant = newHabitant();
        habitant.setDocNumber("fsfsdfsdfsdf");

        String requestBody = om.writeValueAsString(habitant);
        MvcResult mr = this.mockMvc.perform(post("/habitant/").content(requestBody).contentType("application/json"))
                .andExpect(status().isBadRequest()).andReturn();

        String result = mr.getResponse().getContentAsString();
        log.debug("result: {}", result);
        Assert.assertEquals("{\"docNumber\":\"только цифры\"}", result);
    }


    private Habitant newHabitant() {
        Habitant habitant = new Habitant();
        habitant.setName("Василий");
        habitant.setAddress("г. Волобуевск");
        habitant.setBirthDate(LocalDate.now());
        habitant.setDocNumber("11111111111");
        return habitant;
    }
}