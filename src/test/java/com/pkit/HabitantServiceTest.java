package com.pkit;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.pkit.model.Habitant;
import com.pkit.service.HabitantService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.validation.ConstraintViolationException;
import java.time.LocalDate;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
@WebAppConfiguration
@ContextConfiguration(classes = Application.class)
public class HabitantServiceTest {

    private final ObjectMapper om = new ObjectMapper()
            .registerModule(new ParameterNamesModule())
            .registerModule(new Jdk8Module())
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .setDateFormat(new StdDateFormat());

    @Autowired
    HabitantService habitantService;

    /**
     * Буквы в номере
     * @throws Exception
     */
    @Test(expected = ConstraintViolationException.class)
    public void testDocNumberValidation() throws Exception {
        log.debug("testDocNumberValidation: ");

        Habitant habitant = newHabitant();

        habitant.setDocNumber("rrrrrrrr");

        habitantService.add(habitant);
    }

    /**
     * Кол-во меньше 6
     * @throws Exception
     */
    @Test(expected = ConstraintViolationException.class)
    public void testDocNumberValidation2() throws Exception {
        log.debug("testDocNumberValidation2: ");

        Habitant habitant = newHabitant();

        habitant.setDocNumber("12345");

        habitantService.add(habitant);
    }

    /**
     * Имя с пробелами и !
     * @throws Exception
     */
    @Test(expected = ConstraintViolationException.class)
    public void testNameValidation() throws Exception {
        log.debug("testNameValidation: ");

        Habitant habitant = newHabitant();

        habitant.setName("Волобуев Васисуалий!");

        habitantService.add(habitant);
    }

    private Habitant newHabitant() {
        Habitant habitant = new Habitant();
        habitant.setName("Василий");
        habitant.setAddress("г. Волобуевск");
        habitant.setBirthDate(LocalDate.now());
        habitant.setDocNumber("11111111111");
        return habitant;
    }
}