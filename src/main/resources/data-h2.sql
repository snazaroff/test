--Чистим базу
DELETE FROM habitant;

--Добавляем жителей
INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Поликарп Варфаламеевич Перендюгин', DATE '1955-01-31', '12345678', 'д.Малые Волобуйки');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Акакий Владиславович Жуйский', DATE '1958-04-27', '856868', 'г.Большие Волобуйки');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Аристарх Феофанович Лаптев', DATE '1964-06-15', '456456456456', 'пгт. Ромашки');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Захар Васильевич Выпердулькин', DATE '1969-07-15', '876876858', 'пгт. Дальние ебеня');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Клавдия Захаровна Обойкина', DATE '1952-01-16', '756756756', 'пгт. Ближние ебеня');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Сергей Георгиевич Бубукин', DATE '1957-11-21', '3645645', 'д. Париж');

INSERT INTO habitant(name, birth_date, doc_number, address)
VALUES('Михаил Тимофеич Пережопкин', DATE '1962-10-12', '6456456456', 'д. Париж');