DROP TABLE IF EXISTS habitant;

CREATE TABLE habitant (
  habitant_id SMALLINT IDENTITY PRIMARY KEY,
  name VARCHAR2(100),
  address VARCHAR2(200),
  doc_number VARCHAR2(12),
  birth_date DATE
);

CREATE UNIQUE INDEX IF NOT EXISTS ind_habitant_doc_number ON habitant(doc_number);