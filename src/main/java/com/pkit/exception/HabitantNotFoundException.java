package com.pkit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "No such habitant")
public class HabitantNotFoundException extends Exception {

    private final Long habitantId;

    public HabitantNotFoundException(Long habitantId) {
        this.habitantId = habitantId;
    }

    @Override
    public String getMessage() {
        return String.format("No such habitant(%s)", habitantId);
    }
}
