package com.pkit;

import com.pkit.exception.HabitantNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@ControllerAdvice
public class CommonRestControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler({HttpClientErrorException.class })
    protected ResponseEntity<Object> handleHttpClientErrorException(HttpClientErrorException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
    }

    @ExceptionHandler({HttpServerErrorException.class })
    protected ResponseEntity<Object> handleHttpServerErrorException(HttpServerErrorException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getResponseBodyAsString(), new HttpHeaders(), ex.getStatusCode(), request);
    }

    @ExceptionHandler({ConstraintViolationException.class })
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest request) {
        return handleExceptionInternal(ex,  String.format("Нарушение уникального индекса или первичного ключа: %s", ex.getConstraintName()), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler({HabitantNotFoundException.class })
    protected ResponseEntity<Object> handleHabitantNotFoundException(HabitantNotFoundException ex, WebRequest request) {
        return handleExceptionInternal(ex,  ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }
}