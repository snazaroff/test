package com.pkit.repository;

import com.pkit.model.Habitant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

@Repository
public interface HabitantRepository extends JpaRepository<Habitant, Long>, QuerydslPredicateExecutor<Habitant> {}