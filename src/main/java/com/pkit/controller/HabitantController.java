package com.pkit.controller;

import com.pkit.exception.HabitantNotFoundException;
import com.pkit.model.HabitantFilter;
import com.pkit.model.Habitant;
import com.pkit.service.*;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.*;

@Slf4j
@RestController
@RequestMapping("/habitant")
public class HabitantController {

    private final HabitantService habitantService;

    public HabitantController(
            HabitantService habitantService
    ) {
        this.habitantService = habitantService;
    }

    @ApiOperation(value = "Возвращает список жителей", tags = {"habitant"})
    @GetMapping("/")
    List<Habitant> getAllHabitant(
            @ApiParam(name = "name", value = "Имя жителя", required = false) @RequestParam(name = "name", required = false) String name,
            @ApiParam(name = "address", value = "Адрес жителя", required = false) @RequestParam(name = "address", required = false) String address,
            @ApiParam(name = "doc_number", value = "Номер документа жителя", required = false) @RequestParam(name = "doc_number", required = false) String docNumber,
            @RequestParam(name = "page", defaultValue = "1") Integer page,
            @RequestParam(name = "size", defaultValue = "10") Integer size
    ) {
        log.debug("getAllHabitant(page: {}, size: {}, name: {}, address: {}, docNumber: {})", page, size, name, address, docNumber);

        Page<Habitant> habitantPage = habitantService.getAll(new HabitantFilter(name, address, docNumber), PageRequest.of(page - 1, size));
        return habitantPage.getContent();
    }

    @ApiOperation(value = "Возвращает жителя по ID", tags = {"habitant"})
    @GetMapping("/{habitantId}")
    Habitant getHabitant(
            @ApiParam(name = "habitantId", value = "ID жителя", required = true) @PathVariable Long habitantId
    ) throws HabitantNotFoundException {
        log.debug("getHabitant( habitantId: {})", habitantId);

        if (!habitantService.exists(habitantId)) throw new HabitantNotFoundException(habitantId);
        return habitantService.get(habitantId);
    }

    @ApiOperation(value = "Добавляет жителя в базу", tags = {"habitant"})
    @PostMapping
    Habitant saveHabitant(
            @ApiParam(name = "habitant", value = "Житель", required = true) @Valid @RequestBody Habitant habitant
    ) {
        log.debug("saveHabitant: {}", habitant);

        return habitantService.add(habitant);
    }

    @ApiOperation(value = "Редактирует жителя в базе", tags = {"habitant"})
    @PutMapping(path="/{habitantId}", produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    Habitant updateHabitant(
            @ApiParam(name = "habitantId", value = "ID жителя", required = true) @PathVariable Long habitantId,
            @ApiParam(name = "habitant", value = "Житель", required = true) @RequestBody @Valid Habitant habitant
    ) throws HabitantNotFoundException {
        log.debug("updateHabitant( habitantId: {}, habitant: {})", habitantId, habitant);

        if (!habitantService.exists(habitantId)) throw new HabitantNotFoundException(habitantId);
        habitant.setHabitantId(habitantId);
        habitant = habitantService.update(habitant);
        return habitant;
    }

    @ApiOperation(value = "Удаляет жителя из базы", tags = {"habitant"})
    @DeleteMapping("/{habitantId}")
    void deleteHabitant(
            @ApiParam(name = "habitantId", value = "ID жителя", required = true) @PathVariable Long habitantId
    ) {
        log.debug("deleteHabitant( habitantId: {})", habitantId);

        if (habitantService.exists(habitantId))
            habitantService.delete(habitantId);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}