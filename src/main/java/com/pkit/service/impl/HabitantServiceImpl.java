package com.pkit.service.impl;

import com.pkit.model.*;
import com.pkit.repository.*;
import com.pkit.service.HabitantService;
import com.querydsl.core.BooleanBuilder;
import lombok.extern.apachecommons.CommonsLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@CommonsLog
@Service
@Transactional
public class HabitantServiceImpl extends BaseServiceImpl<Habitant, Long> implements HabitantService {

    private final HabitantRepository habitantRepository;

    public HabitantServiceImpl(HabitantRepository habitantRepository) {
        this.habitantRepository = habitantRepository;
    }

    @Override
    @Transactional
    public List<Habitant> getAll() {
        log.debug("getAll()");

        return habitantRepository.findAll();
    }

    @Override
    @Transactional
    public Page<Habitant> getAll(HabitantFilter habitantFilter, Pageable pageable) {
        log.debug("getAll(pageable: " + pageable + ")");

        QHabitant habitant = QHabitant.habitant;
        BooleanBuilder builder = new BooleanBuilder();

        if( habitantFilter.getName() != null && !habitantFilter.getName().trim().equals("")) {
            builder.and(habitant.name.eq(habitantFilter.getName().trim()));
        }
        if( habitantFilter.getAddress() != null && !habitantFilter.getAddress().trim().equals("")) {
            builder.and(habitant.address.eq(habitantFilter.getAddress().trim()));
        }
        if( habitantFilter.getDocNumber() != null && !habitantFilter.getDocNumber().trim().equals("")) {
            builder.and(habitant.docNumber.eq(habitantFilter.getDocNumber().trim()));
        }

        return habitantRepository.findAll(builder.getValue(), pageable);
    }

    @Override
    @Transactional
    public Habitant add(Habitant habitant) {
        log.debug("add: " + habitant);

        return habitantRepository.save(habitant);
    }

    @Override
    @Transactional
    public Habitant update(Habitant habitant) {
        log.debug("update: " + habitant);

        return habitantRepository.save(habitant);
    }
}