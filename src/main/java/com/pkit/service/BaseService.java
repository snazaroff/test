package com.pkit.service;

import java.io.Serializable;
import java.util.Collection;

public interface BaseService<T, E extends Serializable> {

    Collection<T> getAll();

    T get(E id);

    T add(T obj);

    T update(T obj);

    void delete(E id);

    boolean exists(E id);
}
