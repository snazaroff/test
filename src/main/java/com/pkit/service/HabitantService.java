package com.pkit.service;

import com.pkit.model.HabitantFilter;
import com.pkit.model.Habitant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface HabitantService extends BaseService<Habitant, Long> {

    List<Habitant> getAll();

    Page<Habitant> getAll(HabitantFilter habbitantFilter, Pageable pageable);
}