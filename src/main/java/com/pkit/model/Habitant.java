package com.pkit.model;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;

import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Getter
@Setter
@ToString
@Entity
@Table(name = "habitant")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Transactional
public class Habitant implements Serializable {

    @JsonProperty("habitant_id")
    private Long habitantId;

    @NotNull
    @Size(max = 100)
    @Pattern(regexp = "[-a-zA-Zа-яА-я]*", message = "только кириилица, латиница и дефис")
    @JsonProperty("name")
    private String name;

    @JsonProperty("birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthDate;

    @Size(min = 6, max = 12)
    @Pattern(regexp = "[0-9]*", message = "только цифры")
    @JsonProperty("doc_number")
    private String docNumber;

    @Size(max = 200)
    @JsonProperty("address")
    private String address;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "habitant_id", unique = true, nullable = false)
    public Long getHabitantId() {
        return habitantId;
    }

    @Column(name = "name", length = 100)
    public String getName() {
        return name;
    }

    @Column(name = "birth_date")
    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Column(name = "doc_number", length = 12)
    public String getDocNumber() {
        return docNumber;
    }

    @Column(name = "address", length = 200)
    public String getAddress() {
        return address;
    }
}