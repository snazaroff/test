package com.pkit.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class HabitantFilter {
    private String name;
    private String address;
    private String docNumber;

    public HabitantFilter(String name, String address, String docNumber) {
        this.name = name;
        this.address = address;
        this.docNumber = docNumber;
    }
}
