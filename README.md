# test для NTR Lab

База данных h2: jdbc:h2:mem:testdb

Swagger

Basic авторизация: user/123 - можно зайти в swagger, admin/123 - можно дергать методы

Для сложных запросов используется QueryDSL. Возможно более надежный вариант javax.persistence.criteria.*.

Скрипт базы и скрипты тестовых данных статические

Тесты уровня сервисов, только валидация.
Интеграционные тесты - добавление, редактирование, удаление через HTTP.


